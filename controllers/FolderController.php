<?php

namespace frontend\modules\library\controllers;

use Yii;
use emilasp\library\models\Folder;
use emilasp\library\models\search\FolderSearch;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\filters\VerbFilter;

/**
 * LibraryFolderController implements the CRUD actions for LibraryFolder model.
 */
class FolderController extends Controller
{
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'only'  => [
                    'index',
                    'view',
                    'create',
                    'update',
                    'delete',
                    'create-ajax',
                    'move-child',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'create-ajax',
                            'move-child',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                //'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all LibraryFolder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new FolderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LibraryFolder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Folder::className()),
        ]);
    }

    /**
     * Creates a new LibraryFolder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Folder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Updates an existing LibraryFolder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id, Folder::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing LibraryFolder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Folder::className())->delete();

        return $this->redirect(['index']);
    }

    public function actionDeleteAjax($id)
    {
        $this->findModel($id, Folder::className())->deleteWithChildren();

        echo Json::encode(['good']);
        Yii::$app->end();
    }

    /**
     * Создаем ноду ajax
     *
     * @return mixed
     */
    public function actionCreateAjax()
    {
        $model = new Folder();
        $model->setCreate();

        if ($model->load(Yii::$app->request->post())) {
            if ((int)$model->parent > 0) {
                $node = Folder::findOne($model->parent);
                $model->appendTo($node);
            } else {
                $model->makeRoot();
            }

            return '<h2>Папка создана</h2>' . $this->renderAjax('_formModal', [
                    'model' => $model,
                ]);
        } else {
            return $this->renderAjax('_formModal', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Перемещаем ноду
     */
    public function actionMoveChild()
    {
        $id     = Yii::$app->request->post('id', null);
        $parent = Yii::$app->request->post('parent', null);

        $model       = Folder::findOne($id);
        $parentModel = Folder::findOne($parent);

        if ($model) {
            if ((int)$parent > 0) {
                $model->appendTo($parentModel);
            } else {
                $model->makeRoot();
            }
        }
        echo Json::encode(['good']);
        Yii::$app->end();
    }
}
