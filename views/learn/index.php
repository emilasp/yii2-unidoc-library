<?php

use emilasp\library\models\Folder;
use emilasp\library\models\Learn;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use kartik\grid\DataColumn;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\library\models\search\LearnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Html::tag('span', '', ['class' => 'fa fa-list text-primary']) . ' '
    . Yii::t('library', 'Library Learns');
$this->params['breadcrumbs'][] = Yii::t('library', 'Library Learns');
?>
<div class="library-learn-index">

    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-default panel-margin clearfix">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-folder green"></i> Папки:</h3>
                </div>
                <div class="panel panel-margin">
                    <?= CategorySelectTree::widget([
                        'categoryClassName' => Folder::className(),
                        'model'             => $searchModel,
                        'attribute'         => 'category_id',
                        'options'           => [
                            'checkbox'       => false,
                            'selectMode'     => 1,
                            'minExpandLevel' => 3,
                            'select'         => 'function(event, data) {
                                var selNodes = data.tree.getSelectedNodes();
                                var selKeys = $.map(selNodes, function(node){return node.key;});
                                var category_id = "";
                                if ($.isArray(selKeys) && selKeys.length) {
                                    category_id = selKeys[0];
                                }
                                $.pjax({
                                    container:"#library-list",
                                    "timeout" : 0,
                                    push:true,
                                    "data":{"LearnSearch":{"category_id":category_id}},
                                    scrollTo: false
                                });
                                $("#library-list").loading(true);
                            }',
                        ]
                    ]) ?>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <?php Pjax::begin(['id' => 'library-list', 'scrollTo' => false]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel'  => $searchModel,
                'columns'      => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'format'    => 'raw',
                        'value'     => function ($model, $key, $index, $column) {
                            return Html::a($model->name, ['view', 'id' => $model->id],
                                ['class' => '', 'data-pjax' => 0]);
                        },
                        'class'     => DataColumn::className(),
                        'hAlign'    => GridView::ALIGN_LEFT,
                        'vAlign'    => GridView::ALIGN_MIDDLE,
                        'width'     => '250px',
                    ],
                    [
                        'attribute' => 'category_id',
                        'class'     => DataColumn::className(),
                        'value'     => function ($model, $key, $index, $column) {
                            return $model->category->name;
                        },
                        'filter'    => Folder::find()->map()->cache()
                            ->where(['created_by' => Yii::$app->user->id])->orderBy('name')->all(),
                    ],

                    [
                        'attribute' => 'type',
                        'class'     => DataColumn::className(),
                        'value'     => function ($model, $key, $index, $column) {
                            return Learn::$types[$model->type];
                        },
                        'filter'    => Learn::$types
                    ],
                    [
                        'attribute' => 'formTags',
                        'class'     => DataColumn::className(),
                        'value'     => function ($model, $key, $index, $column) {
                            $tags = implode(',', ArrayHelper::getColumn($model->tags, 'name'));
                            return \emilasp\core\helpers\StringHelper::truncate($tags, 20);
                        },
                    ],

                    'learn',
                    [
                        'attribute' => 'status',
                        'class'     => DataColumn::className(),
                        'value'     => function ($model, $key, $index, $column) {
                            return Learn::$statuses[$model->status];
                        },
                        'filter'    => Learn::$statuses
                    ],

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
