<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\library\models\LibraryLearn */

$this->title = Html::tag('span', '', ['class' => 'fa fa-plus text-success']) . ' ' . Yii::t('library', 'Create Library Learn');
$this->params['breadcrumbs'][] = ['label' => Yii::t('library', 'Library Learns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('library', 'Create Library Learn');
?>
<div class="library-learn-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
