<?php

use dosamigos\ckeditor\CKEditor;
use dosamigos\selectize\SelectizeTextInput;
use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use emilasp\library\models\Folder;
use emilasp\library\models\Learn;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\modules\library\models\Learn */
/* @var $form yii\widgets\ActiveForm */

\conquer\codemirror\CodemirrorAsset::register($this);
?>
    <div class="library-learn-form">

        <?php $form = ActiveForm::begin([
            'id'          => 'learn-form',
            'fieldConfig' => ['autoPlaceholder' => false],
            'formConfig'  => ['deviceSize' => 'sm']
        ]); ?>

        <?= $form->errorSummary($model, ['header' => '']); ?>

        <div class="row">
            <div class="col-md-8">
                <?= $form->field($model, 'name', [
                    'addon' => [
                        'groupOptions' => ['class' => 'input-group-sm'],
                        'prepend'      => ['content' => '<i class="fa fa-edit"></i>']
                    ]
                ])->textInput(['maxlength' => 150]) ?>

                <?php Pjax::begin(['id' => 'codemirror-input']); ?>

                <?php if ($model->type == Learn::TYPE_TEXT) : ?>

                    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                        'options' => ['rows' => 3],
                        'preset'  => 'standart',
                    ]) ?>

                <?php else: ?>
                    <?= $form->field($model, 'description')->widget(CodemirrorWidget::className(), [
                        'type'     => $model->type,
                        'options'  => ['rows' => 20],
                        'settings' => ['lineWrapping' => true],
                    ]) ?>
                <?php endif; ?>

                <?php Pjax::end(); ?>


                <?= $form->field($model, 'formTags')->widget(SelectizeTextInput::className(), [
                    'loadUrl'       => ['/taxonomy/tag/search'],
                    'options'       => ['class' => 'form-control'],
                    'clientOptions' => [
                        'plugins'     => ['remove_button', 'drag_drop', 'restore_on_backspace'],
                        'valueField'  => 'name',
                        'labelField'  => 'name',
                        'searchField' => ['name'],
                        'create'      => true,
                    ],
                ])->hint('Используйте запятые для разделения меток') ?>

                <?= $form->field($model, 'comment')->textarea() ?>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
                    </div>
                    <div class="panel-body">
                        <?= FileInputWidget::widget([
                            'model'         => $model,
                            'type'          => FileInputWidget::TYPE_MULTI,
                            'title'         => true,
                            'description'   => true,
                            'showTitle'     => true,
                            'previewHeight' => 20,
                        ]) ?>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'type', [
                            'addon' => [
                                'groupOptions' => ['class' => 'input-group-sm'],
                                'prepend'      => ['content' => '<i class="fa fa-gavel"></i>']
                            ]
                        ])->dropDownList($model::$types, ['data-value' => $model->type]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'status', [
                            'addon' => [
                                'groupOptions' => ['class' => 'input-group-sm'],
                                'prepend'      => ['content' => '<i class="fa fa-lock"></i>']
                            ]
                        ])->dropDownList($model::$statuses) ?>
                    </div>
                </div>
                <div class="panel panel-default panel-margin clearfix">
                    <div class="panel-heading">
                        <h3 class="panel-title"><span class="fa fa-folder"></span> Папки:</h3>
                    </div>
                    <div class="panel panel-margin">
                        <?= $form->field($model, 'category_id')->widget(CategorySelectTree::className(), [
                            'categoryClassName' => Folder::className(),
                            'options'           => [
                                'select' => 'function(event, data) {
                                    var selNodes = data.tree.getSelectedNodes();
                                    var selKeys = $.map(selNodes, function(node){return node.key;});
                                    var category_id = "";
                                    if ($.isArray(selKeys) && selKeys.length) {
                                        $("#{{selectorFiled}}-input").val(selKeys.join(", "));
                                    }
    
                                    //console.log($("#{{selectorFiled}}-input").val());
                                }',
                            ]

                        ])->label(false) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'learn')->checkbox() ?>
                    </div>
                    <div class="col-md-8">
                        <?= $form->field($model, 'remember', [
                            'addon' => [
                                'groupOptions' => ['class' => 'input-group-sm'],
                                'prepend'      => ['content' => '<i class="fa fa-signal"></i>']
                            ]
                        ])->textInput(['maxlength' => 150]) ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-group">
            <?= Html::submitButton(
                $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
            ) ?>
        </div>

        <?php \kartik\form\ActiveForm::end(); ?>

    </div>

<?php
$this->registerJs(
    <<<JS
    $('body').on('change', '#learn-type', function () {
        var type = $(this).val();
        var oldType = $(this).data('value');
        
        var description = '';

        if (oldType == 1) {
            description = $('#learn-description').val();
        } else {
            description = codemirror.getValue();
        }
        
        $("#codemirror-input").loading(true);
        
        $.pjax({
            container:"#codemirror-input",
            type: 'POST',
            "timeout" : 0,
            push:false,
            "data":{"Learn":{"type":type, "description":description}},
            scrollTo: false
        });
    })
JS
); ?>