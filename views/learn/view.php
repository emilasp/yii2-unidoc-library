<?php

use emilasp\core\extensions\CodemirrorWidget\CodemirrorWidget;
use emilasp\library\models\Learn;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Markdown;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model emilasp\library\models\Learn */

$this->title                   = Html::tag('span', '', ['class' => 'fa fa-eye text-info']) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('library', 'Library Learns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="library-learn-view">

    <div class="clearfix">

        <p class="float-right">
            <?= Html::a(Yii::t('site', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('site', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data'  => [
                    'confirm' => Yii::t('site', 'Are you sure you want to delete this item?'),
                    'method'  => 'post',
                ],
            ]) ?>
        </p>
    </div>

    <div class="row">
        <div class="col-md-8">

            <?php if ($model->type == Learn::TYPE_TEXT) : ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= $model->description ?>
                    </div>
                </div>
            <?php elseif ($model->type == Learn::TYPE_CODE_MARKDOWN) : ?>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?= Markdown::process($model->description) ?>
                    </div>
                </div>
            <?php else: ?>
                <?= CodemirrorWidget::widget([
                    'name'     => 'description',
                    'value'    => $model->description,
                    'type'     => $model->type,
                    'settings' => ['readOnly' => true, 'lineWrapping' => true],
                    'options'  => ['rows' => 39],
                ]) ?>
            <?php endif; ?>

        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Информация:</h3>
                </div>
                <div class="panel">
                    <?= DetailView::widget([
                        'model'      => $model,
                        'attributes' => [
                            'name',
                            [
                                'attribute' => 'category_id',
                                'value'     => $model->category->name,
                            ],
                            [
                                'attribute' => 'formTags',
                                'value'     => implode(', ', ArrayHelper::getColumn($model->tags, 'name')),
                            ],
                            [
                                'attribute' => 'type',
                                'value'     => Learn::$types[$model->type],
                                'format'    => ['raw', []],
                            ],
                            [
                                'attribute' => 'status',
                                'value'     => Learn::$statuses[$model->status],
                                'format'    => ['raw', []],
                            ],
                            'comment:ntext',
                            'remember',
                        ],
                    ]) ?>
                </div>
            </div>

            <hr/>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
                </div>
                <div class="panel-body">
                    <?= FileInputWidget::widget([
                        'model'         => $model,
                        'type'          => FileInputWidget::TYPE_MULTI,
                        'title'         => true,
                        'description'   => true,
                        'showTitle'     => true,
                        'previewHeight' => 20,
                        'onlyView'      => true,
                    ]) ?>
                </div>
            </div>

        </div>
    </div>
</div>
