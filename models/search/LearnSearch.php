<?php

namespace emilasp\library\models\search;

use emilasp\library\models\Folder;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\library\models\Learn;

/**
 * LibraryLearnSearch represents the model behind the search form about `\frontend\modules\library\models\LibraryLearn`.
 */
class LearnSearch extends Learn
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'category_id', 'type', 'remember', 'status'], 'integer'],
            [['name', 'description', 'comment', 'updated_at', 'created_by', 'created_at', 'formTags'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Learn::find();
        $query->with(['author', 'category']);
        $query->joinWith('tags');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'pageSize' => Yii::$app->params['grid']['pageSize'],
            ],
            'sort'       => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->category_id) {
            $category = Folder::findOne($this->category_id);
            if ($category) {
                $ids = $category->leaves()->select(['id'])->asArray()->column();//->groupBy('"1"')
                if ($ids) {
                    $ids[]              = $this->category_id;
                    $query->andFilterWhere(['category_id' => $ids]);
                } else {
                    $query->andFilterWhere(['category_id' => $this->category_id]);
                }
            }
        }

        if ($this->name) {
            $query->andFilterWhere(['ilike', $this::tableName() . '.name', $this->name])
                ->orFilterWhere(['ilike', 'description', $this->name])
                ->orFilterWhere(['ilike', 'taxonomy_tag.name', $this->name])
                ->orFilterWhere(['ilike', 'comment', $this->name]);
        }

        if ($this->formTags) {
            $query->filterWhere(['ilike', 'taxonomy_tag.name', $this->formTags]);
        }

        if ($this->status === null) {
            $this->status = self::STATUS_ACTIVE;
        }

        $query->andFilterWhere([
            self::tableName() . '.id'         => $this->id,
            self::tableName() . '.type'       => $this->type,
            self::tableName() . '.remember'   => $this->remember,
            self::tableName() . '.status'     => $this->status,
            self::tableName() . '.updated_at' => $this->updated_at,
            self::tableName() . '.created_at' => $this->created_at,
        ]);

        return $dataProvider;
    }
}
