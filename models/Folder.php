<?php
namespace emilasp\library\models;

use emilasp\taxonomy\models\Category;


/**
 * This is the model class for table "library_folder".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property integer $image_id
 * @property integer $type
 * @property integer $status
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Folder extends Category
{
    const ROOT_CATEGORY = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_library_folder';
    }
}
