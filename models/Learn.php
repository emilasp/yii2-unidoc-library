<?php

namespace emilasp\library\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\models\File;
use emilasp\taxonomy\behaviors\TagBehavior;
use emilasp\taxonomy\models\Tag;
use emilasp\taxonomy\models\TagLink;
use Yii;
use emilasp\users\common\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "library_learn".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $description
 * @property string  $comment
 * @property integer $category_id
 * @property integer $type
 * @property integer $remember
 * @property integer $status
 * @property string  $updated_at
 * @property string  $created_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User    $author
 * @property Folder  $folder
 * @property User    $updatedBy
 */
class Learn extends ActiveRecord
{
    const TYPE_CODE_MARKDOWN = 9;
    const TYPE_CODE_PHP      = 10;
    const TYPE_CODE_PHP_HTML = 15;
    const TYPE_CODE_JS       = 11;
    const TYPE_CODE_CSS      = 12;
    const TYPE_CODE_SQL      = 13;
    const TYPE_CODE_XML      = 14;
    const TYPE_CODE_BASH     = 16;
    const TYPE_TEXT          = 1;
    const TYPE_VIDEO         = 2;
    const TYPE_FILE          = 3;
    const TYPE_OTHER         = 5;

    public static $types = [
        self::TYPE_CODE_MARKDOWN => 'MarkDown',
        self::TYPE_CODE_PHP      => 'PHP',
        self::TYPE_CODE_PHP_HTML => 'HTML',
        self::TYPE_CODE_JS       => 'JS',
        self::TYPE_CODE_CSS      => 'CSS',
        self::TYPE_CODE_SQL      => 'SQL',
        self::TYPE_CODE_XML      => 'XML',
        self::TYPE_CODE_BASH     => 'Bash',
        self::TYPE_TEXT          => 'Текст',
        self::TYPE_VIDEO         => 'Видео',
        self::TYPE_FILE          => 'Файл',
        self::TYPE_OTHER         => 'Другое',
    ];

    const STATUS_SEARCH   = 2;
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 0;

    public static $statuses = [
        self::STATUS_ACTIVE   => 'Виден',
        self::STATUS_SEARCH   => 'В поиске',
        self::STATUS_INACTIVE => 'Скрыт',
    ];

    public $learn;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'tags'  => [
                'class' => TagBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unidoc_library_learn';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'comment'], 'string'],
            [['name', 'description', 'category_id', 'type', 'status'], 'required'],
            [['category_id', 'type', 'remember', 'status', 'created_by', 'updated_by'], 'integer'],
            [['updated_at', 'created_at', 'created_by', 'updated_by', 'learn', 'formTags'], 'safe'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('library', 'ID'),
            'category_id' => Yii::t('library', 'Папка'),
            'name'        => Yii::t('library', 'Наименование'),
            'description' => Yii::t('library', 'Знание'),
            'comment'     => Yii::t('library', 'Комментарий'),
            'type'        => Yii::t('library', 'Тип'),
            'remember'    => Yii::t('library', 'Изучено'),
            'status'      => Yii::t('library', 'Статус'),
            'updated_at'  => Yii::t('library', 'Изменён'),
            'created_at'  => Yii::t('library', 'Создан'),
            'created_by'  => Yii::t('library', 'Кем создан'),
            'updated_by'  => Yii::t('library', 'Кем изменён'),
            'learn'       => Yii::t('library', 'Изучено'),
            'formTags'    => Yii::t('taxonomy', 'Tags'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Folder::className(), ['id' => 'category_id']);
    }

    /** Событие "перед сохранением"
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->learn) {
                if (is_numeric($this->remember)) {
                    $this->remember++;
                } else {
                    $this->remember = 1;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Формируем пресет для Codemirror
     *
     * @return string
     */
    public function getCodemirrorPreset()
    {
        return 'MODE_' . self::$types[$this->type];
    }

    /**
     * @return Query
     */
    public function getTagLink()
    {
        return $this->hasMany(TagLink::className(), ['object_id' => 'id'])
            ->andOnCondition(['object' => self::className()]);
    }

    /**
     * @return Query
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->via('tagLink');
    }

    /**
     * Получаем файлы привязанные к моделе
     *
     * @return Query
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])->where(['object' => self::className()]);
    }
}
