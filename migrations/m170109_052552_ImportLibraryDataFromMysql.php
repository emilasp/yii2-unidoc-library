<?php

use emilasp\library\models\Folder;
use emilasp\taxonomy\models\Category;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170109_052552_ImportLibraryDataFromMysql extends Migration
{
    private $time;
    private $memory;


    public function up()
    {
        $this->db->createCommand('truncate table unidoc_library_learn CASCADE')->execute();

        /** @var \yii\db\Connection $oldDb */
        $oldDb = Yii::$app->db_old;


        $rootCategory             = new Folder();
        $rootCategory->code       = 'library_root';
        $rootCategory->name       = 'Библиотека';
        $rootCategory->text       = 'Библиотека';
        $rootCategory->short_text = 'Библиотека';
        $rootCategory->type       = 1;
        $rootCategory->status     = 1;
        $rootCategory->makeRoot();


        $folders = $oldDb->createCommand('SELECT * FROM library_folder')->queryAll();
        foreach ($folders as $folder) {
            echo $folder['name'] . PHP_EOL;

            $tree     = $folder['tree'] + 1;
            $folderId = $folder['id'] + 1;

            $code = 'category_' . $folderId;

            $sqlInsertLearn = <<<SQL
            INSERT INTO unidoc_library_folder (id, code, name, text, short_text, image_id, type, status, tree, lft, rgt, depth, created_at, updated_at, created_by, updated_by) 
            VALUES 
            ({$folderId}, '{$code}', '{$folder['name']}', '{$folder['description']}', '{$folder['description']}', null, {$folder['type']}, 1, {$tree}, {$folder['lft']}, {$folder['rgt']}, {$folder['depth']}, '{$folder['created_at']}', '{$folder['updated_at']}', '{$folder['created_by']}', '{$folder['updated_by']}');
SQL;
            $this->db->createCommand($sqlInsertLearn)->execute();

            $sqlInsertLearn = <<<SQL
           SELECT setval('unidoc_library_folder_id_seq', (SELECT MAX(id) + 1 FROM unidoc_library_folder));
SQL;

            $this->db->createCommand($sqlInsertLearn)->execute();
        }


        $roots = Folder::find()->roots()->all();


        foreach ($roots as $root) {
            if ($root->id !== 1) {
                $root->appendTo($rootCategory);
            }
        }


        $learns = $oldDb->createCommand('SELECT * FROM library_learn')->queryAll();
        foreach ($learns as $learn) {

            $folderId = $learn['folder_id'] + 1;
            echo $learn['id'] . '-' . $folderId . PHP_EOL;

            $sqlInsertLearn = <<<SQL
            INSERT INTO unidoc_library_learn
            (id, name, description, comment, category_id, type, remember, status, updated_at, created_at, created_by, updated_by)
            VALUES
            ({$learn['id']}, '{$learn['name']}', :description, '{$learn['comment']}', {$folderId}, {$learn['type']}, 0, {$learn['status']}, '{$learn['updated_at']}', '{$learn['created_at']}', '{$learn['created_by']}', '{$learn['updated_by']}');
SQL;
            $this->db->createCommand($sqlInsertLearn)->bindValues([':description' => $learn['description']])->execute();
        }

        $this->afterMigrate();
        return true;
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->beforeMigrate();
    }


    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
