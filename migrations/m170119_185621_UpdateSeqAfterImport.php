<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m170119_185621_UpdateSeqAfterImport extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $sql = <<<SQL
        SELECT setval('unidoc_library_folder_id_seq', (SELECT MAX(id) + 1 FROM unidoc_library_folder));
SQL;

        $this->db->createCommand($sql)->execute();

        $sql = <<<SQL
        SELECT setval('unidoc_library_learn_id_seq', (SELECT MAX(id) + 1 FROM unidoc_library_learn));
SQL;
        $this->db->createCommand($sql)->execute();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->afterMigrate();
        return false;
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
