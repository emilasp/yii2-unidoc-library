<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m150327_211934_init
 */
class m150327_211934_init extends Migration
{
    public $tableOptions = null;

    public function up()
    {
        $this->createTable('unidoc_library_folder', [
            'id'             => $this->primaryKey(11),
            'code'           => $this->string(255)->unique(),
            'name'           => $this->string(255)->notNull(),
            'text'           => $this->text()->notNull(),
            'short_text'     => $this->text()->notNull(),
            'image_id'       => $this->integer(11),
            'type'           => $this->smallInteger(2)->notNull(),
            'status'         => $this->smallInteger(1)->notNull(),
            'tree'           => $this->integer(),
            'lft'            => $this->integer()->notNull(),
            'rgt'            => $this->integer()->notNull(),
            'depth'          => $this->integer()->notNull(),
            'created_at'     => $this->dateTime(),
            'updated_at'     => $this->dateTime(),
            'created_by'     => $this->integer(11),
            'updated_by'     => $this->integer(11),
        ], $this->tableOptions);

        /* $this->addForeignKey(
             'fk_taxonomy_category_image_id',
             'taxonomy_category',
             'image_id',
             'files_file',
             'id'
         );*/

        $this->addForeignKey(
            'fk_library_folder_created_by',
            'unidoc_library_folder',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_library_folder_updated_by',
            'unidoc_library_folder',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('library_folder_type', 'unidoc_library_folder', 'type');




        $this->createTable('unidoc_library_learn', [
            'id'          => $this->primaryKey(11),
            'name'        => $this->string(150)->null(),
            'description' => $this->text()->null(),
            'comment'     => $this->text()->null(),
            'category_id' => $this->integer(11)->unsigned()->notNull(),
            'type'        => $this->smallInteger(1)->unsigned()->notNull(),
            'remember'    => $this->smallInteger(4)->unsigned(),
            'status'      => $this->smallInteger(1)->unsigned()->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ]);

        $this->addForeignKey(
            'fk_unidoc_library_learn_created_by',
            'unidoc_library_learn',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_unidoc_library_learn_updated_by',
            'unidoc_library_learn',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_unidoc_library_learn_folder_id',
            'unidoc_library_learn',
            'category_id',
            'unidoc_library_folder',
            'id'
        );
    }

    public function down()
    {
        $this->dropTable('unidoc_library_learn');
        $this->dropTable('unidoc_library_folder');
        return true;
    }


    public function init()
    {
        parent::init();
    }
}
