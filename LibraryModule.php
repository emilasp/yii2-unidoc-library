<?php
namespace emilasp\library;

use emilasp\core\CoreModule;

/**
 * Class LibraryModule
 * @package emilasp\library
 */
class LibraryModule extends CoreModule
{
    public $defaultRoute = 'library';
    public $controllerNamespace = 'emilasp\library\controllers';

    public $routeAfterLogin = '';

    public function init()
    {
        parent::init();
    }
}
